import { Component } from '@angular/core';
import { PloneService } from '../shared/index';
import { Router, NavigationEnd } from '@angular/router';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css'],
})

export class HomeComponent {

  html: string = '';
  content: any;

  constructor(public ploneService: PloneService, private router: Router) {
    router.events.subscribe((route) => {
      if(route instanceof NavigationEnd) {
        this.loadContent(route.url);
      }
    });
  }

  loadContent(path: string = '') {
    this.ploneService
    .get(path)
    .subscribe((content) => {
      this.content = content;
      if(content && content.text && content.text['content-type'] == 'text/html') {
        this.html = this.content.text.data;
      } else {
        this.html = 'No results were found.';
      }
    });
  }

}
