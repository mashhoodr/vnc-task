import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/**
 * This class provides the NameList service with methods to read names and add names.
 */
@Injectable()
export class PloneService {
  state: any;
  subject: Subject<any>;
  options: any = {};
  baseUrl: string = 'http://kitconcept.com:19080/Plone';

  constructor(private http: Http) {
  }

  private getHeaders(){
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    return headers;
  }

  get(path: string = '') {
    return this.http
      .get(`${this.baseUrl}${path}`, {headers: this.getHeaders()})
      .map((res) => {
        let result = res.json();
        // adjust the links without the base url
        if(result.items) {
          result.items = result.items.map((item) => {
            item['@id'] = item['@id'].replace(this.baseUrl, ''); 
            return item;
          });
        }
        return result;
      });
  }

}
