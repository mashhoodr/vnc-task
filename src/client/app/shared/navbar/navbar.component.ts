import { Component } from '@angular/core';
import { PloneService } from '../plone/index';

/**
 * This class represents the navigation bar component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.css'],
})

export class NavbarComponent {
  elements: any = [];
  constructor(private ploneService: PloneService) {}

  ngOnInit() {
    this.ploneService
      .get()
      .subscribe(result => this.elements = result.items);
  }
}
