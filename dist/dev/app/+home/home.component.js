"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('../shared/index');
var router_1 = require('@angular/router');
var HomeComponent = (function () {
    function HomeComponent(ploneService, router) {
        var _this = this;
        this.ploneService = ploneService;
        this.router = router;
        this.html = '';
        router.events.subscribe(function (route) {
            if (route instanceof router_1.NavigationEnd) {
                _this.loadContent(route.url);
            }
        });
    }
    HomeComponent.prototype.loadContent = function (path) {
        var _this = this;
        if (path === void 0) { path = ''; }
        this.ploneService
            .get(path)
            .subscribe(function (content) {
            _this.content = content;
            if (content && content.text && content.text['content-type'] == 'text/html') {
                _this.html = _this.content.text.data;
            }
            else {
                _this.html = 'No results were found.';
            }
        });
    };
    HomeComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'sd-home',
            templateUrl: 'home.component.html',
            styleUrls: ['home.component.css'],
        }), 
        __metadata('design:paramtypes', [index_1.PloneService, router_1.Router])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC8raG9tZS9ob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQTBCLGVBQWUsQ0FBQyxDQUFBO0FBQzFDLHNCQUE2QixpQkFBaUIsQ0FBQyxDQUFBO0FBQy9DLHVCQUFzQyxpQkFBaUIsQ0FBQyxDQUFBO0FBWXhEO0lBS0UsdUJBQW1CLFlBQTBCLEVBQVUsTUFBYztRQUx2RSxpQkEwQkM7UUFyQm9CLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUhyRSxTQUFJLEdBQVcsRUFBRSxDQUFDO1FBSWhCLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQUMsS0FBSztZQUM1QixFQUFFLENBQUEsQ0FBQyxLQUFLLFlBQVksc0JBQWEsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xDLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzlCLENBQUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxtQ0FBVyxHQUFYLFVBQVksSUFBaUI7UUFBN0IsaUJBV0M7UUFYVyxvQkFBaUIsR0FBakIsU0FBaUI7UUFDM0IsSUFBSSxDQUFDLFlBQVk7YUFDaEIsR0FBRyxDQUFDLElBQUksQ0FBQzthQUNULFNBQVMsQ0FBQyxVQUFDLE9BQU87WUFDakIsS0FBSSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7WUFDdkIsRUFBRSxDQUFBLENBQUMsT0FBTyxJQUFJLE9BQU8sQ0FBQyxJQUFJLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUMxRSxLQUFJLENBQUMsSUFBSSxHQUFHLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUNyQyxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ04sS0FBSSxDQUFDLElBQUksR0FBRyx3QkFBd0IsQ0FBQztZQUN2QyxDQUFDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBL0JIO1FBQUMsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsU0FBUztZQUNuQixXQUFXLEVBQUUscUJBQXFCO1lBQ2xDLFNBQVMsRUFBRSxDQUFDLG9CQUFvQixDQUFDO1NBQ2xDLENBQUM7O3FCQUFBO0lBNEJGLG9CQUFDO0FBQUQsQ0ExQkEsQUEwQkMsSUFBQTtBQTFCWSxxQkFBYSxnQkEwQnpCLENBQUEiLCJmaWxlIjoiYXBwLytob21lL2hvbWUuY29tcG9uZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBQbG9uZVNlcnZpY2UgfSBmcm9tICcuLi9zaGFyZWQvaW5kZXgnO1xuaW1wb3J0IHsgUm91dGVyLCBOYXZpZ2F0aW9uRW5kIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcblxuLyoqXG4gKiBUaGlzIGNsYXNzIHJlcHJlc2VudHMgdGhlIGxhenkgbG9hZGVkIEhvbWVDb21wb25lbnQuXG4gKi9cbkBDb21wb25lbnQoe1xuICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICBzZWxlY3RvcjogJ3NkLWhvbWUnLFxuICB0ZW1wbGF0ZVVybDogJ2hvbWUuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnaG9tZS5jb21wb25lbnQuY3NzJ10sXG59KVxuXG5leHBvcnQgY2xhc3MgSG9tZUNvbXBvbmVudCB7XG5cbiAgaHRtbDogc3RyaW5nID0gJyc7XG4gIGNvbnRlbnQ6IGFueTtcblxuICBjb25zdHJ1Y3RvcihwdWJsaWMgcGxvbmVTZXJ2aWNlOiBQbG9uZVNlcnZpY2UsIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpIHtcbiAgICByb3V0ZXIuZXZlbnRzLnN1YnNjcmliZSgocm91dGUpID0+IHtcbiAgICAgIGlmKHJvdXRlIGluc3RhbmNlb2YgTmF2aWdhdGlvbkVuZCkge1xuICAgICAgICB0aGlzLmxvYWRDb250ZW50KHJvdXRlLnVybCk7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxuICBsb2FkQ29udGVudChwYXRoOiBzdHJpbmcgPSAnJykge1xuICAgIHRoaXMucGxvbmVTZXJ2aWNlXG4gICAgLmdldChwYXRoKVxuICAgIC5zdWJzY3JpYmUoKGNvbnRlbnQpID0+IHtcbiAgICAgIHRoaXMuY29udGVudCA9IGNvbnRlbnQ7XG4gICAgICBpZihjb250ZW50ICYmIGNvbnRlbnQudGV4dCAmJiBjb250ZW50LnRleHRbJ2NvbnRlbnQtdHlwZSddID09ICd0ZXh0L2h0bWwnKSB7XG4gICAgICAgIHRoaXMuaHRtbCA9IHRoaXMuY29udGVudC50ZXh0LmRhdGE7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmh0bWwgPSAnTm8gcmVzdWx0cyB3ZXJlIGZvdW5kLic7XG4gICAgICB9XG4gICAgfSk7XG4gIH1cblxufVxuIl19
