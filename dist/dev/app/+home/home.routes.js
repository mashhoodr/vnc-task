"use strict";
var index_1 = require('./index');
exports.HomeRoutes = [
    {
        path: '**',
        component: index_1.HomeComponent
    }
];

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC8raG9tZS9ob21lLnJvdXRlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0Esc0JBQThCLFNBQVMsQ0FBQyxDQUFBO0FBRTNCLGtCQUFVLEdBQVk7SUFDakM7UUFDRSxJQUFJLEVBQUUsSUFBSTtRQUNWLFNBQVMsRUFBRSxxQkFBYTtLQUN6QjtDQUNGLENBQUMiLCJmaWxlIjoiYXBwLytob21lL2hvbWUucm91dGVzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xuaW1wb3J0IHsgSG9tZUNvbXBvbmVudCB9IGZyb20gJy4vaW5kZXgnO1xuXG5leHBvcnQgY29uc3QgSG9tZVJvdXRlczogUm91dGVbXSA9IFtcbiAge1xuICAgIHBhdGg6ICcqKicsXG4gICAgY29tcG9uZW50OiBIb21lQ29tcG9uZW50XG4gIH1cbl07XG4iXX0=
