"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/map');
require('rxjs/add/operator/catch');
var PloneService = (function () {
    function PloneService(http) {
        this.http = http;
        this.options = {};
        this.baseUrl = 'http://kitconcept.com:19080/Plone';
    }
    PloneService.prototype.getHeaders = function () {
        var headers = new http_1.Headers();
        headers.append('Accept', 'application/json');
        return headers;
    };
    PloneService.prototype.get = function (path) {
        var _this = this;
        if (path === void 0) { path = ''; }
        return this.http
            .get("" + this.baseUrl + path, { headers: this.getHeaders() })
            .map(function (res) {
            var result = res.json();
            if (result.items) {
                result.items = result.items.map(function (item) {
                    item['@id'] = item['@id'].replace(_this.baseUrl, '');
                    return item;
                });
            }
            return result;
        });
    };
    PloneService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], PloneService);
    return PloneService;
}());
exports.PloneService = PloneService;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC9zaGFyZWQvcGxvbmUvcGxvbmUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEscUJBQTJCLGVBQWUsQ0FBQyxDQUFBO0FBQzNDLHFCQUF3QyxlQUFlLENBQUMsQ0FBQTtBQUd4RCxRQUFPLHVCQUF1QixDQUFDLENBQUE7QUFDL0IsUUFBTyx5QkFBeUIsQ0FBQyxDQUFBO0FBTWpDO0lBTUUsc0JBQW9CLElBQVU7UUFBVixTQUFJLEdBQUosSUFBSSxDQUFNO1FBSDlCLFlBQU8sR0FBUSxFQUFFLENBQUM7UUFDbEIsWUFBTyxHQUFXLG1DQUFtQyxDQUFDO0lBR3RELENBQUM7SUFFTyxpQ0FBVSxHQUFsQjtRQUNFLElBQUksT0FBTyxHQUFHLElBQUksY0FBTyxFQUFFLENBQUM7UUFDNUIsT0FBTyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztRQUM3QyxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQ2pCLENBQUM7SUFFRCwwQkFBRyxHQUFILFVBQUksSUFBaUI7UUFBckIsaUJBY0M7UUFkRyxvQkFBaUIsR0FBakIsU0FBaUI7UUFDbkIsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJO2FBQ2IsR0FBRyxDQUFDLEtBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFNLEVBQUUsRUFBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFDLENBQUM7YUFDM0QsR0FBRyxDQUFDLFVBQUMsR0FBRztZQUNQLElBQUksTUFBTSxHQUFHLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUV4QixFQUFFLENBQUEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDaEIsTUFBTSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxVQUFDLElBQUk7b0JBQ25DLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUksQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7b0JBQ3BELE1BQU0sQ0FBQyxJQUFJLENBQUM7Z0JBQ2QsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDO1lBQ0QsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUNoQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUE5Qkg7UUFBQyxpQkFBVSxFQUFFOztvQkFBQTtJQWdDYixtQkFBQztBQUFELENBL0JBLEFBK0JDLElBQUE7QUEvQlksb0JBQVksZUErQnhCLENBQUEiLCJmaWxlIjoiYXBwL3NoYXJlZC9wbG9uZS9wbG9uZS5zZXJ2aWNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cCwgSGVhZGVycywgUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9odHRwJztcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL09ic2VydmFibGUnO1xuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMvU3ViamVjdCc7XG5pbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL21hcCc7XG5pbXBvcnQgJ3J4anMvYWRkL29wZXJhdG9yL2NhdGNoJztcblxuLyoqXG4gKiBUaGlzIGNsYXNzIHByb3ZpZGVzIHRoZSBOYW1lTGlzdCBzZXJ2aWNlIHdpdGggbWV0aG9kcyB0byByZWFkIG5hbWVzIGFuZCBhZGQgbmFtZXMuXG4gKi9cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBQbG9uZVNlcnZpY2Uge1xuICBzdGF0ZTogYW55O1xuICBzdWJqZWN0OiBTdWJqZWN0PGFueT47XG4gIG9wdGlvbnM6IGFueSA9IHt9O1xuICBiYXNlVXJsOiBzdHJpbmcgPSAnaHR0cDovL2tpdGNvbmNlcHQuY29tOjE5MDgwL1Bsb25lJztcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHApIHtcbiAgfVxuXG4gIHByaXZhdGUgZ2V0SGVhZGVycygpe1xuICAgIGxldCBoZWFkZXJzID0gbmV3IEhlYWRlcnMoKTtcbiAgICBoZWFkZXJzLmFwcGVuZCgnQWNjZXB0JywgJ2FwcGxpY2F0aW9uL2pzb24nKTtcbiAgICByZXR1cm4gaGVhZGVycztcbiAgfVxuXG4gIGdldChwYXRoOiBzdHJpbmcgPSAnJykge1xuICAgIHJldHVybiB0aGlzLmh0dHBcbiAgICAgIC5nZXQoYCR7dGhpcy5iYXNlVXJsfSR7cGF0aH1gLCB7aGVhZGVyczogdGhpcy5nZXRIZWFkZXJzKCl9KVxuICAgICAgLm1hcCgocmVzKSA9PiB7XG4gICAgICAgIGxldCByZXN1bHQgPSByZXMuanNvbigpO1xuICAgICAgICAvLyBhZGp1c3QgdGhlIGxpbmtzIHdpdGhvdXQgdGhlIGJhc2UgdXJsXG4gICAgICAgIGlmKHJlc3VsdC5pdGVtcykge1xuICAgICAgICAgIHJlc3VsdC5pdGVtcyA9IHJlc3VsdC5pdGVtcy5tYXAoKGl0ZW0pID0+IHtcbiAgICAgICAgICAgIGl0ZW1bJ0BpZCddID0gaXRlbVsnQGlkJ10ucmVwbGFjZSh0aGlzLmJhc2VVcmwsICcnKTsgXG4gICAgICAgICAgICByZXR1cm4gaXRlbTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgfSk7XG4gIH1cblxufVxuIl19
